import React, { useLayoutEffect, useRef } from "react";
import PropTypes from "prop-types";
import ReactDOM from "react-dom";
import "./styles.scss";

const propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  closeOnOutsideClick: PropTypes.bool,
  isOpen: PropTypes.bool,
  size: PropTypes.oneOf(["small", "medium", "large"]),
  onClose: PropTypes.func,
};

const Modal = ({
  children,
  className = "",
  closeOnOutsideClick = true,
  isOpen = false,
  size = "medium",
  onClose,
}) => {
  const modalRef = useRef();
  const portalRef = useRef();

  useLayoutEffect(() => {
    // Create divs for portal and overlay
    portalRef.current = document.createElement("div");

    document.body.appendChild(portalRef.current);

    return () => {
      document.body.removeChild(portalRef.current);
    };
  }, []);

  if (!isOpen) {
    return null;
  }

  return ReactDOM.createPortal(
    <div
      className="overlay"
      onClick={e => {
        if (closeOnOutsideClick && e.target.className === "overlay") {
          onClose();
        }
      }}
      ref={modalRef}
    >
      <div className={`overlay__modal overlay__modal--${size} ${className}`}>
        {children}
      </div>
    </div>,
    portalRef.current,
  );
};

Modal.propTypes = propTypes;

export default Modal;
