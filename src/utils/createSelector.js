const createSelector = fn => {
  let memoizedValue;
  let prevArguments = [];

  return (...newArguments) => {
    if (
      newArguments.length === prevArguments.length &&
      newArguments.every((arg, i) => arg === prevArguments[i])
    ) {
      return memoizedValue;
    }

    prevArguments = newArguments;
    memoizedValue = fn(...newArguments);

    return memoizedValue;
  };
};

export default createSelector;
