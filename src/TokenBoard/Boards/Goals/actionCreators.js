import {
  CREATE_GOAL,
  DELETE_GOAL,
  AWARD_TOKEN,
  HIDE_GOAL,
  SHOW_GOAL,
} from "./actionTypes";

export const awardToken = (goalId, boardId) => ({
  type: AWARD_TOKEN,
  payload: { goalId, boardId },
});

export const createGoal = (goal, board) => ({
  type: CREATE_GOAL,
  payload: {
    boardId: board.id,
    title: goal.title,
    tokensNeeded: goal.tokensNeeded,
    reward: goal.reward,
  },
});

export const deleteGoal = (goalId, boardId) => ({
  type: DELETE_GOAL,
  payload: { goalId, boardId },
});

export const hideGoal = (goalId, boardId) => ({
  type: HIDE_GOAL,
  payload: { goalId, boardId },
});

export const showGoal = (goalId, boardId) => ({
  type: SHOW_GOAL,
  payload: { goalId, boardId },
});
