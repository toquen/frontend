const createDefaultAction = (type, payload) => ({
  type,
  payload,
});

export default createDefaultAction;
