import React from "react";
import PropTypes from "prop-types";
import { AppState } from "App";
import Button from "components/Button";
import Card from "components/Card";
import Text from "components/Text";
import Link from "components/Link";
import createDefaultAction from "utils/createDefaultAction";
import useRequest from "hooks/useRequest";
import useReducerWithMiddleware from "hooks/useReducerWithMiddleware";
import AddNewGoal from "./AddNewGoal";
import Goal from "./Goal";
import reducer, { getInitialState } from "./reducer";
import getMiddleware from "./getMiddleware";
import {
  TOGGLE_HIDDEN_GOALS,
  TOGGLE_NEW_GOAL_MODAL,
  GOALS_RESPONSE_RECEIVED,
  HIDDEN_GOALS_RESPONSE_RECEIVED,
} from "./actionTypes";
import { selectSortedGoals, selectSortedHiddenGoals } from "./selectors";
import "./styles.scss";

const propTypes = {
  board: PropTypes.object.isRequired,
};

const Goals = ({ board }) => {
  const { api } = React.useContext(AppState);
  const [showHiddenGoals, setShowHiddenGoals] = React.useState(false);
  const goalsResponse = useRequest({
    method: "GET",
    path: `/boards/${board.id}/goals`,
  });
  const hiddenGoalsResponse = useRequest({
    method: "GET",
    path: `/boards/${board.id}/goals/hidden`,
    skip: showHiddenGoals,
  });
  const middleware = React.useMemo(
    () => [
      getMiddleware(api, goalsResponse.refetch, hiddenGoalsResponse.refetch),
    ],
    [api, goalsResponse.refetch, hiddenGoalsResponse.refetch],
  );
  const [state, dispatch] = useReducerWithMiddleware(
    reducer,
    middleware,
    {},
    getInitialState,
  );

  React.useEffect(() => {
    dispatch(createDefaultAction(GOALS_RESPONSE_RECEIVED, goalsResponse.data));
  }, [dispatch, goalsResponse.data]);

  React.useEffect(() => {
    dispatch(
      createDefaultAction(
        HIDDEN_GOALS_RESPONSE_RECEIVED,
        hiddenGoalsResponse.data,
      ),
    );
  }, [dispatch, hiddenGoalsResponse.data]);

  React.useEffect(() => {
    // hiddenGoalsResponse needs to be defined before middleware but depends on
    // the value of state.showHiddenGoals.
    // state.showHiddenGoals is only defined afterwards.
    // So this hack mirrors the value of state.showHiddenGoals to local state.
    setShowHiddenGoals(state.showHiddenGoals);
  }, [state.showHiddenGoals]);

  const goals = selectSortedGoals(state);
  const hiddenGoals = selectSortedHiddenGoals(state);

  return (
    <div className="goals">
      <Text tag="h1">
        <Link to="/boards">All Token Boards</Link>/
        <Text tag="span">{board.name}</Text>
      </Text>
      <div>
        <h2 className="goals__header">Goals</h2>
        <ul className="goals__list">
          {goalsResponse.loading && (
            <li className="goals__loader loader">Loading...</li>
          )}
          {!goalsResponse.loading && (
            <React.Fragment>
              <AddGoalButton dispatch={dispatch} />
              {goals.map(goal => (
                <li key={goal.id}>
                  <Goal
                    board={board}
                    className="goals__goal"
                    goal={goal}
                    dispatch={dispatch}
                  />
                </li>
              ))}
            </React.Fragment>
          )}
        </ul>
        <h2 className="goals__header">
          Hidden goals{" "}
          <Button
            label={state && state.showHiddenGoals ? "Hide" : "Show"}
            onClick={() => dispatch(createDefaultAction(TOGGLE_HIDDEN_GOALS))}
            theme="secondary"
          />
        </h2>
        {state.showHiddenGoals && (
          <ul className="goals__list goals__list--hidden">
            {hiddenGoals.length === 0 && <Text tag="li">No hidden goals</Text>}
            {hiddenGoals.map(goal => (
              <li key={goal.id}>
                <Goal
                  className="goals__goal goals__goal--hidden"
                  board={board}
                  goal={goal}
                  dispatch={dispatch}
                />
              </li>
            ))}
          </ul>
        )}
      </div>
      <AddNewGoal
        dispatch={dispatch}
        board={board}
        showModal={state.showNewGoalModal}
        onCloseModal={() =>
          dispatch(createDefaultAction(TOGGLE_NEW_GOAL_MODAL))
        }
      />
    </div>
  );
};

Goals.propTypes = propTypes;

export default Goals;

const AddGoalButton = ({ dispatch }) => {
  return (
    <li onClick={() => dispatch(createDefaultAction(TOGGLE_NEW_GOAL_MODAL))}>
      <Card className="goals__goal goals__goal--empty goals__add-new-goal goals__add-new-goal">
        <span role="img" aria-label="+">
          ➕
        </span>
        &nbsp;Add New Goal
      </Card>
    </li>
  );
};
