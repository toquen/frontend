import React from "react";
import { Link, BrowserRouter, Route, Switch, NavLink } from "react-router-dom";
import "./App.scss";
import TokenBoard from "./TokenBoard";
import Home from "./Home";
import Register from "./Register";
import Login from "./Login";
import appReducer from "appState/reducer";
import getInitialState from "appState/getInitialState";
import getAppMiddleware from "appState/getMiddleware";
import useApi from "hooks/useApi";
import useReducerWithMiddleware from "hooks/useReducerWithMiddleware";
import createDefaultAction from "utils/createDefaultAction";
import { INIT } from "appState/actionTypes";
import NotFound from "NotFound";
import Avatar from "Avatar";
import Logout from "Logout";
import Account from "Account";

export const AppState = React.createContext({});

function App() {
  const apiRef = React.useRef();
  const middleware = React.useMemo(() => [getAppMiddleware(apiRef)], []);
  const [appState, appDispatch] = useReducerWithMiddleware(
    appReducer,
    middleware,
    {},
    getInitialState,
  );
  const api = useApi(appState.authToken);
  const contextValue = React.useMemo(
    () => ({
      appState,
      appDispatch,
      api,
    }),
    [appDispatch, appState, api],
  );

  React.useEffect(() => {
    apiRef.current = api;
  }, [api]);

  React.useEffect(() => {
    appDispatch(createDefaultAction(INIT));
  }, [appDispatch]);

  if (!appState.initialized) {
    return null;
  }

  return (
    <BrowserRouter>
      <AppState.Provider value={contextValue}>
        <div className="app">
          <header className="app__header">
            <h1 className="app__title">
              <Link className="app__title" to="/">
                Toquen.ca
              </Link>
            </h1>
            <nav className="app__nav">
              {appState.user && (
                <NavLink
                  className="app__nav-link"
                  activeClassName="app__nav-link--selected"
                  to="/boards"
                >
                  Boards
                </NavLink>
              )}
              <NavLink
                className="app__nav-link"
                activeClassName="app__nav-link--selected"
                to="/help"
              >
                Help
              </NavLink>
              <NavLink
                className="app__nav-link"
                activeClassName="app__nav-link--selected"
                to="/contact"
              >
                Contact Us
              </NavLink>
            </nav>
            <div className="app__user-links">
              {!appState.user && (
                <Link className="app__nav-link" to="/login">
                  Log in
                </Link>
              )}
              {appState.user && (
                <div className="app__nav-link">
                  <Avatar name={appState.user.name} />
                </div>
              )}
            </div>
          </header>
          <Switch>
            {appState.user && (
              <Route
                path="/boards"
                render={({ match }) => (
                  <TokenBoard
                    className="app__body app__body--fixed-height"
                    match={match}
                  />
                )}
              />
            )}
            {appState.user && (
              <Route
                path="/account"
                render={({ match }) => (
                  <Account className="app__body" match={match} />
                )}
              />
            )}
            <Route
              path="/register"
              render={() => <Register className="app__body" />}
            />
            <Route
              path="/login"
              render={() => <Login className="app__body" />}
            />
            <Route path="/logout">
              <Logout user={appState.user} dispatch={appDispatch} />
            </Route>
            <Route
              path="/"
              exact
              render={() => <Home className="app__body" />}
            />
            <Route render={() => <NotFound className="app__body" />} />
          </Switch>
        </div>
      </AppState.Provider>
    </BrowserRouter>
  );
}

export default App;
