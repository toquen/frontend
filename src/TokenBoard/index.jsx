import React from "react";
import { Route, Switch } from "react-router-dom";
import createDefaultAction from "utils/createDefaultAction";
import useReducerWithMiddleware from "hooks/useReducerWithMiddleware";
import { AppState } from "App";
import Sidebar from "./Sidebar";
import reducer, { getInitialState } from "./reducer";
import getApiMiddleware from "./getApiMiddleware";
import { selectSelectedBoardTokens, selectSelectedBoard } from "./selectors";
import LargeGoal from "./LargeGoal";
import { GET_BOARDS } from "./actionTypes";
import Tokens from "./Tokens";
import Boards from "./Boards";
import "./styles.scss";

const TokenBoard = ({ className, match }) => {
  const { api } = React.useContext(AppState);
  const middlewares = React.useMemo(() => [getApiMiddleware(api)], [api]);
  const [state, dispatch] = useReducerWithMiddleware(
    reducer,
    middlewares,
    {},
    getInitialState,
  );

  React.useEffect(() => {
    dispatch(createDefaultAction(GET_BOARDS));
  }, [dispatch]);

  return (
    <div className={`token-board ${className}`}>
      <Sidebar
        className="token-board__available-tokens"
        tokens={selectSelectedBoardTokens(state)}
        boards={state.boards}
      />
      <Switch>
        <Route
          path="/goals/tokens"
          render={() => {
            const board = selectSelectedBoard(state);

            if (!board) {
              return null;
            }

            return (
              <Tokens
                className="token-board__tokens"
                dispatch={dispatch}
                tokens={selectSelectedBoardTokens(state)}
              />
            );
          }}
        />
        <Route
          path="/goals/:goalId"
          render={({ match }) => {
            const board = selectSelectedBoard(state);

            if (!board) {
              return null;
            }

            const goal = board.goals && board.goals[match.params.goalId];

            return (
              <LargeGoal
                className="token-board__goals"
                dispatch={dispatch}
                goal={goal}
              />
            );
          }}
        />
        <Route
          path="/boards"
          render={() => (
            <Boards
              className="token-board__goals"
              boards={state.boards}
              dispatch={dispatch}
            />
          )}
        />
      </Switch>
    </div>
  );
};

export default TokenBoard;
