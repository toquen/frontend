import { createSelector } from "reselect";

export const selectSelectedBoard = state => {
  const { selectedBoardId } = state;

  if (!state.boards) {
    return null;
  }

  return state.boards[selectedBoardId] || null;
};

export const selectGoalsArray = createSelector(selectSelectedBoard, board => {
  if (!board) {
    return [];
  }

  return Object.values(board.goals);
});

export const selectSelectedBoardTokens = createSelector(
  selectSelectedBoard,
  board => {
    if (!board) {
      return null;
    } else {
      return Object.values(board.tokens);
    }
  },
);

export const selectVisibleGoalsArray = createSelector(
  selectGoalsArray,
  goals => {
    return goals.filter(goal => !goal.isHidden);
  },
);

export const selectHiddenGoalsArray = createSelector(
  selectGoalsArray,
  goals => {
    return goals.filter(goal => goal.isHidden);
  },
);
