import React from "react";
import PropTypes from "prop-types";
import Text from "components/Text";
import Input from "components/Input";
import FormDialog from "components/Modal/FormDialog";
import { createBoard } from "TokenBoard/actionCreators";

const propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
};

const NewBoardModal = ({ dispatch, isOpen, onClose }) => {
  const [boardName, setBoardName] = React.useState("");
  const [learnerName, setLearnerName] = React.useState("");

  const handleSubmit = event => {
    event.preventDefault();
    dispatch(createBoard(boardName, learnerName));
    onClose();
  };

  return (
    <FormDialog
      buttons={[
        {
          label: "Cancel",
          onClick: onClose,
          theme: "secondary",
          type: "button",
        },
        { label: "Add", theme: "primary", type: "submit" },
      ]}
      className="add-new-board"
      closeOnOutsideClick={true}
      isOpen={isOpen}
      onClose={onClose}
      onSubmit={handleSubmit}
      title="Let's Add a New Board"
    >
      <Input
        name="board-name"
        onChange={({ target }) => setBoardName(target.value)}
        placeholder="Name your Token Board"
        value={boardName}
      />
      <Input
        name="learner-name"
        onChange={({ target }) => setLearnerName(target.value)}
        placeholder="Learner's name"
        value={learnerName}
      />
    </FormDialog>
  );
};

NewBoardModal.propTypes = propTypes;

export default NewBoardModal;
