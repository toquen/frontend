import React from "react";
import Token from "components/Token";
import "./styles.scss";

const Tokens = ({ className, tokens }) => {
  return (
    <div className={`tokens ${className}`}>
      <h2 className="tokens__header">Tokens</h2>
      <ul>
        {tokens.map(token => (
          <li className="tokens__token" key={token.id}>
            <Token className="tokens__display" icon={token.icon} size="large" />
            <span className="tokens__name">{token.name}</span>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Tokens;
