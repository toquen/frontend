import React from "react";
import { Redirect } from "react-router-dom";
import createDefaultAction from "utils/createDefaultAction";
import { USER_LOGGED_OUT } from "appState/actionTypes";

const Logout = ({ dispatch, user }) => {
  React.useEffect(() => {
    if (user) {
      dispatch(createDefaultAction(USER_LOGGED_OUT));
    }
  }, [user, dispatch]);

  if (user) {
    return "Logging out";
  }

  return <Redirect to="/" />;
};

export default Logout;
