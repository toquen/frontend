import React from "react";
import "./styles.scss";
import { Link } from "react-router-dom";

const Home = ({ className }) => {
  return (
    <div className={`${className} home`}>
      <h2 className="home__header">The token board app</h2>
      <main className="home__description">
        <p>
          Easy-to-use and customizable for your needs. Add and remove goals as
          you please. Free for single token boards and premium features
          available at an affordable rate.
        </p>
      </main>
      <div className="home__packages">
        <article className="home__feature-comparison">
          <p className="home__package-name">Basic features:</p>
          <ul className="home__feature-list">
            <li className="home__feature-item">
              Create fun goals to challenge learners
            </li>
            <li className="home__feature-item">
              Use our built-in attractive tokens or upload your own
            </li>
            <li className="home__feature-item">Free!</li>
          </ul>
          <Link className="home__register-button" to="/register">
            Register now
          </Link>
        </article>
        <article className="home__feature-comparison">
          <p className="home__package-name">Premium features:</p>
          <ul className="home__feature-list">
            <li className="home__feature-item">Manage multiple token boards</li>
            <li className="home__feature-item">
              Give access to multiple users
            </li>
            <li className="home__feature-item">
              Analytics and metrics for goal completion, token selection, and
              more
            </li>
          </ul>
          <button className="home__register-button home__register-button--disabled">
            Available Soon
          </button>
        </article>
      </div>
    </div>
  );
};

export default Home;
