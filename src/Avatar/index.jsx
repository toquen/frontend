import React from "react";
import { Link, useLocation } from "react-router-dom";
import "./styles.scss";

const Avatar = props => {
  const containerRef = React.createRef();
  const [dropdownVisible, setDropdownVisible] = React.useState(false);
  const location = useLocation();

  React.useEffect(() => {
    const hideDropdown = e => {
      if (
        containerRef.current &&
        containerRef.current !== e.target &&
        !containerRef.current.contains(e.target)
      ) {
        setDropdownVisible(false);
      }
    };

    document.addEventListener("click", hideDropdown);

    return () => {
      document.removeEventListener("click", hideDropdown);
    };
  }, [containerRef]);

  React.useEffect(() => {
    setDropdownVisible(false);
  }, [location]);

  const onClickIcon = () => {
    setDropdownVisible(!dropdownVisible);
  };

  return (
    <div className="avatar" ref={containerRef}>
      <span
        className="avatar__username"
        onClick={onClickIcon}
        role="img"
        aria-label={props.name}
      >
        👤
      </span>
      {dropdownVisible && (
        <ul className="avatar__dropdown">
          <li className="avatar__link">
            <Link className="avatar__link" to="/account">
              {props.name}
            </Link>
          </li>
          <li className="avatar__link">
            <Link className="avatar__link" to="/logout">
              Log out
            </Link>
          </li>
        </ul>
      )}
    </div>
  );
};

export default Avatar;
