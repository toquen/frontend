import React from "react";
import { Link as RouterLink } from "react-router-dom";
import "./styles.scss";

const Link = ({ className = "", to, children }) => {
  return (
    <RouterLink className={`link ${className}`} to={to}>
      {children}
    </RouterLink>
  );
};

export default Link;
