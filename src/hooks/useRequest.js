import React from "react";
import PropTypes from "prop-types";
import { AppState } from "App";

const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;

const propTypes = {
  body: PropTypes.object,
  method: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
  skip: PropTypes.bool,
};
const useRequest = ({ body, method, path, skip = false }) => {
  const {
    appState: { authToken },
  } = React.useContext(AppState);
  const [error, setError] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState(null);
  const ref = React.useRef();

  ref.current = { authToken, body, loading, method, path };

  const refetch = React.useCallback(refetchBody => {
    if (ref.current.loading) {
      return;
    }

    let requestBody;
    try {
      requestBody = JSON.stringify(refetchBody);
    } catch (err) {
      setError(err);
      return;
    }

    setLoading(true);

    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        if (xhr.readyState !== 4) {
          return;
        }

        setLoading(false);
        setData(xhr.response);

        if (xhr.status < 200 && xhr.status >= 400) {
          const err = new Error(xhr.status);
          setError(err);
          reject(err);
        } else {
          resolve(xhr.response);
        }
      };

      xhr.open(ref.current.method, `${API_BASE_URL}${ref.current.path}`);
      xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
      if (ref.current.authToken) {
        xhr.setRequestHeader(
          "Authorization",
          `Bearer ${ref.current.authToken}`,
        );
      }
      xhr.responseType = "json";
      xhr.send(requestBody);
    });
  }, []);

  React.useEffect(() => {
    if (skip) {
      return;
    }

    let requestBody;
    try {
      requestBody = JSON.stringify(body);
    } catch (err) {
      setError(err);
      return;
    }

    const xhr = request({
      authToken,
      method,
      path,
      requestBody,
      setData,
      setError,
      setLoading,
    });

    return () => {
      if (loading) {
        xhr.abort();
      }
    };

    // We don't care if `body` nor `params` objects are different as long as
    // they represent the same data
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [authToken, JSON.stringify(body), method, skip, path]);

  return { error, loading, data, refetch };
};

useRequest.propTypes = propTypes;

export default useRequest;

const request = ({
  authToken,
  method,
  path,
  requestBody,
  setData,
  setError,
  setLoading,
}) => {
  setLoading(true);

  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = () => {
    if (xhr.readyState !== 4) {
      return;
    }

    if (xhr.status < 200 && xhr.status >= 400) {
      setError(new Error(xhr.status));
    }

    setLoading(false);
    setData(xhr.response);
  };

  xhr.open(method, `${API_BASE_URL}${path}`);
  xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
  if (authToken) {
    xhr.setRequestHeader("Authorization", `Bearer ${authToken}`);
  }
  xhr.responseType = "json";
  xhr.send(requestBody);

  return xhr;
};
