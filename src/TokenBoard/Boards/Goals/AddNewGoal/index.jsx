import React from "react";
import Modal from "components/Modal";
import { createGoal } from "../actionCreators";
import "./styles.scss";
import Button from "components/Button";

const AddNewGoal = ({ dispatch, board, showModal, onCloseModal }) => {
  const onSubmit = event => {
    event.preventDefault();

    const { elements } = event.target;

    const goal = Array.from(elements).reduce((acc, el) => {
      acc[el.name] = el.value;
      return acc;
    }, {});

    dispatch(createGoal(goal, board));
  };

  return (
    <Modal className="add-new-goal" isOpen={showModal} onClose={onCloseModal}>
      <h2 className="add-new-goal__header">Let's Add a New Goal</h2>
      <form className="add-new-goal__form" onSubmit={onSubmit}>
        <input
          className="add-new-goal__field"
          name="title"
          placeholder="Enter goal title"
        />
        <input
          className="add-new-goal__field"
          name="tokensNeeded"
          placeholder="Number of tokens required"
          type="number"
        />
        <textarea
          className="add-new-goal__field add-new-goal__field--multiline"
          name="reward"
          placeholder="Reward for completing the goal"
        />
        <footer className="add-new-goal__footer">
          <Button
            className="add-new-goal__footer-button add-new-goal__footer-button--secondary"
            label="Cancel"
            theme="secondary"
            onClick={onCloseModal}
          />
          <Button
            className="add-new-goal__footer-button add-new-goal__footer-button--primary"
            type="submit"
            label="Done"
          />
        </footer>
      </form>
    </Modal>
  );
};

export default AddNewGoal;
