import {
  GET_GOALS,
  GET_TOKENS,
  AWARD_TOKEN,
  GET_BOARDS,
  SET_ERROR,
  HIDE_GOAL,
  REQUEST_HIDE_GOAL,
  TOGGLE_HIDDEN_GOALS,
  CREATE_BOARD,
} from "./actionTypes";
import createMiddleware from "utils/createMiddleware";
import { updateGoals, updateBoards, updateBoardTokens } from "./actionCreators";
import createDefaultAction from "utils/createDefaultAction";
import { ERROR_NO_BOARDS, GENERAL_SERVER_ERROR } from "./errors";
import { selectSelectedBoard } from "./selectors";

const getApiMiddleware = api => {
  const dataHandlers = {
    [AWARD_TOKEN]: async (dispatch, state, action) => {
      const goalId = action.payload;
      const { selectedBoardId } = state;

      let response;
      try {
        response = await api.awardToken(selectedBoardId, goalId);
      } catch (e) {
        return;
      }

      dispatch(updateGoals([response]));
    },

    [CREATE_BOARD]: async (dispatch, state, { payload }) => {
      const { boardName, learnerName } = payload;
      const success = await api
        .createBoard(boardName, learnerName)
        .catch(error => null);

      if (success) {
        dispatch(createDefaultAction(GET_BOARDS));
      } else {
        dispatch(createDefaultAction(SET_ERROR, GENERAL_SERVER_ERROR));
      }
    },

    [GET_GOALS]: async (dispatch, state, action) => {
      const goals = await api.getGoals(action.payload);
      dispatch(updateGoals(goals));
    },

    [GET_TOKENS]: async (dispatch, state, { payload: boardId }) => {
      const tokens = await api.getTokens(boardId);
      dispatch(updateBoardTokens(boardId, tokens));
    },

    [GET_BOARDS]: async dispatch => {
      const boards = await api.getBoards();

      if (boards.length) {
        dispatch(updateBoards(boards));
      } else {
        dispatch(createDefaultAction(SET_ERROR, ERROR_NO_BOARDS));
      }
    },

    [REQUEST_HIDE_GOAL]: async (dispatch, state, { payload: goalId }) => {
      const { selectedBoardId } = state;
      try {
        await api.hideGoal(selectedBoardId, goalId);
      } catch (e) {
        dispatch(createDefaultAction(SET_ERROR, GENERAL_SERVER_ERROR));
        return;
      }

      dispatch(createDefaultAction(HIDE_GOAL, goalId));
    },

    [TOGGLE_HIDDEN_GOALS]: async (dispatch, state) => {
      const selectedBoard = selectSelectedBoard(state);

      if (!selectedBoard.showHiddenGoals) {
        const hiddenGoals = await api.getHiddenGoals(selectedBoard.id);
        dispatch(updateGoals(hiddenGoals));
      }
    },
  };

  return createMiddleware(dataHandlers);
};

export default getApiMiddleware;
