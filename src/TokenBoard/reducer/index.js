import {
  UPDATE_GOALS,
  REMOVE_GOAL,
  UPDATE_BOARDS,
  UPDATE_BOARD_TOKENS,
  HIDE_GOAL,
  TOGGLE_HIDDEN_GOALS,
  SET_SELECTED_BOARD,
  BOARD_CREATED,
} from "../actionTypes";
import createReducer from "utils/createReducer";
import { getInitialGoalState, getInitialBoardState } from "./getInitialState";
import { selectSelectedBoard } from "TokenBoard/selectors";

const handlers = {
  [BOARD_CREATED]: (state, { payload: board }) => {
    return {
      ...state,
      boards: {
        ...state.boards,
        [board.id]: board,
      },
    };
  },

  [HIDE_GOAL]: (state, { payload: goalId }) => {
    const selectedBoard = selectSelectedBoard(state);
    const goal = selectedBoard.goals[goalId];

    return {
      ...state,
      boards: {
        ...state.boards,
        [selectedBoard.id]: {
          ...selectedBoard,
          goals: {
            ...selectedBoard.goals,
            [goalId]: {
              ...goal,
              isHidden: true,
            },
          },
        },
      },
    };
  },

  [REMOVE_GOAL]: (state, { payload: goalIdToRemove }) => {
    const selectedBoard = selectSelectedBoard(state);
    const { [goalIdToRemove]: goalToRemove, ...goals } = selectedBoard.goals;

    return {
      ...state,
      boards: {
        ...state.boards,
        [selectedBoard.id]: {
          ...selectedBoard,
          goals,
        },
      },
    };
  },

  [SET_SELECTED_BOARD]: (state, { payload: selectedBoardId }) => ({
    ...state,
    selectedBoardId,
  }),

  [TOGGLE_HIDDEN_GOALS]: state => {
    const selectedBoard = selectSelectedBoard(state);

    return {
      ...state,
      boards: {
        ...state.boards,
        [selectedBoard.id]: {
          ...selectedBoard,
          showHiddenGoals: !selectedBoard.showHiddenGoals,
        },
      },
    };
  },

  [UPDATE_BOARDS]: (state, { payload }) => {
    const { boards, selectedBoardId } = payload;

    return {
      ...state,
      boards: boards.reduce((acc, board) => {
        const boardState = getInitialBoardState(board);
        acc[boardState.id] = boardState;
        return acc;
      }, {}),
      selectedBoardId,
    };
  },

  [UPDATE_GOALS]: (state, { payload: goals }) => {
    const selectedBoard = selectSelectedBoard(state);
    const goalsMap = goals.reduce((acc, goal) => {
      const goalState = getInitialGoalState(goal);
      acc[goalState.id] = goalState;
      return acc;
    }, {});

    return {
      ...state,
      boards: {
        ...state.boards,
        [selectedBoard.id]: {
          ...selectedBoard,
          goals: {
            ...selectedBoard.goals,
            ...goalsMap,
          },
        },
      },
    };
  },

  [UPDATE_BOARD_TOKENS]: (state, action) => {
    return {
      ...state,
      tokens: action.payload,
    };
  },
};

export default createReducer(handlers);

export { default as getInitialState } from "./getInitialState";
