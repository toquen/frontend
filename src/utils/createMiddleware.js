const createMiddleware = handlers => (dispatch, state, action) => {
  if (handlers[action.type]) {
    handlers[action.type](dispatch, state, action);
  }
};

export default createMiddleware;
