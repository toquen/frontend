import {
  GET_TOKENS,
  GET_GOALS,
  UPDATE_BOARD_TOKENS,
  UPDATE_GOALS,
  UPDATE_BOARDS,
  CREATE_BOARD,
  BOARD_CREATED,
} from "./actionTypes";

export const boardCreated = board => ({
  type: BOARD_CREATED,
  payload: board,
});
export const createBoard = (boardName, learnerName) => ({
  type: CREATE_BOARD,
  payload: {
    boardName,
    learnerName,
  },
});
export const getGoals = boardId => ({
  type: GET_GOALS,
  payload: boardId,
});
export const getTokens = boardId => ({
  type: GET_TOKENS,
  payload: boardId,
});
export const updateGoals = goals => ({
  type: UPDATE_GOALS,
  payload: goals,
});
export const updateBoardTokens = (boardId, tokens) => ({
  type: UPDATE_BOARD_TOKENS,
  payload: tokens,
});
export const updateBoards = (boards, selectedBoardId = null) => ({
  type: UPDATE_BOARDS,
  payload: {
    boards,
    selectedBoardId,
  },
});
