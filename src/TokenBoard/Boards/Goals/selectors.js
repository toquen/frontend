import { createSelector } from "reselect";

export const selectGoals = state => state.goals;
export const selectHiddenGoals = state => state.hiddenGoals;

export const selectSortedGoals = createSelector(selectGoals, goals => {
  return Object.values(goals || {});
});

export const selectSortedHiddenGoals = createSelector(
  selectHiddenGoals,
  hiddenGoals => Object.values(hiddenGoals || {}),
);
