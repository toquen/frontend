import React from "react";
import PropTypes from "prop-types";
import Modal from "./index";
import Text from "components/Text";
import Button from "components/Button";
import "./formDialog.scss";

const propTypes = {
  buttons: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      onClick: PropTypes.func,
      theme: PropTypes.oneOf(["primary", "secondary"]),
      type: PropTypes.oneOf(["button", "submit", "reset"]).isRequired,
    }),
  ),
  children: PropTypes.node,
  className: PropTypes.string,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  title: PropTypes.string,
};

const FormDialog = ({
  buttons = [],
  children,
  className = "",
  isOpen,
  onClose,
  onSubmit,
  title = "",
}) => {
  return (
    <Modal
      className={`modal-form-dialog ${className}`}
      isOpen={isOpen}
      onClose={onClose}
    >
      {title !== "" && (
        <Text className="modal-form-dialog__title" size="l" tag="h2">
          {title}
        </Text>
      )}
      <form className="modal-form-dialog__form" onSubmit={onSubmit}>
        <div className="modal-form-dialog__body">{children}</div>
        <div className="modal-form-dialog__footer">
          {buttons.map(
            ({ onClick, label, theme = "primary", type = "button" }, index) => (
              <Button
                className="modal-form-dialog__footer-button"
                key={`${index}-${type}`}
                label={label}
                onClick={onClick}
                theme={theme}
                type={type}
              />
            ),
          )}
        </div>
      </form>
    </Modal>
  );
};

FormDialog.propTypes = propTypes;

export default FormDialog;
