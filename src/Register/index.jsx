import React, { useContext } from "react";
import Button from "components/Button";
import Input from "components/Input";
import { AppState } from "App";
import "./styles.scss";

const Register = () => {
  const { api } = useContext(AppState);
  const [registrationError, setError] = React.useState();
  const { createAccount } = api;

  const onSubmit = async event => {
    event.preventDefault();

    const { target } = event;

    const name = target["account-name"].value;
    const email = target["account-email"].value;
    const password = target["account-password"].value;
    const boardName = target["board-name"].value;

    return createAccount({ name, email, password, boardName }).catch(err => {
      setError(err);
    });
  };

  return (
    <div className="register">
      <form className="register__form" onSubmit={onSubmit}>
        <legend className="register__title">
          Register and Create a New Token Board
        </legend>
        <fieldset className="register__fieldset register__fieldset--account">
          <legend>Account Details</legend>
          <Input placeholder="Account holder's name" name="account-name" />
          <Input
            placeholder="Your email address"
            name="account-email"
            type="email"
          />
          <Input
            placeholder="Your password"
            name="account-password"
            type="password"
          />
        </fieldset>
        <fieldset className="register__fieldset register__fieldset--board">
          <legend>Token Board Details</legend>
          <Input placeholder="Name your Token Board" name="board-name" />
          <Input placeholder="Learner's name" name="learner-name" />
        </fieldset>
        {registrationError && (
          <div className="register__error">Error while registering</div>
        )}
        <Button className="register__button" label="Complete" type="submit" />
      </form>
    </div>
  );
};

export default Register;
