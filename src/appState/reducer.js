import createReducer from "utils/createReducer";
import { USER_LOGGED_IN, USER_LOGGED_OUT, INITIALIZED } from "./actionTypes";

const handlers = {
  [INITIALIZED]: state => ({
    ...state,
    initialized: true,
  }),

  [USER_LOGGED_IN]: (state, { payload }) => {
    const { name, token, userId } = payload;

    return {
      ...state,
      authToken: token,
      user: {
        name,
        id: userId,
      },
    };
  },

  [USER_LOGGED_OUT]: state => ({
    ...state,
    authToken: null,
    user: null,
  }),
};

export default createReducer(handlers);
