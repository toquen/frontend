import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Smiley from "./Smiley";
import "./styles.scss";

const iconToComponentMap = {
  smiley: Smiley,
};

const propTypes = {
  className: PropTypes.string,
  icon: PropTypes.oneOf(["smiley"]),
  size: PropTypes.oneOf(["small", "medium", "large"]),
};

const Token = ({ className = "", icon, size }) => {
  const classes = classNames("token", className, `token--${size}`);

  const Icon = iconToComponentMap[icon];

  if (!Icon) {
    return null;
  }

  return (
    <i className={classes}>
      <Icon />
    </i>
  );
};

Token.propTypes = propTypes;

export default Token;
