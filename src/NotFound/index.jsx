import React from "react";
import { Link } from "react-router-dom";

const NotFound = ({ className }) => {
  return (
    <div className={`not-found ${className}`}>
      <h2 className="not-found__header">Lost?</h2>
      <p>Check that you put in the right URL</p>
      <p>
        Return to the <Link to="/">home page</Link>.
      </p>
    </div>
  );
};

export default NotFound;
