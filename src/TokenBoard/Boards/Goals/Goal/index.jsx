import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Classnames from "classnames";
import Button from "components/Button";
import Card from "components/Card";
import Modal from "components/Modal";
import { awardToken, deleteGoal, hideGoal, showGoal } from "../actionCreators";
import "./styles.scss";

const propTypes = {
  board: PropTypes.object.isRequired,
  className: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
  goal: PropTypes.object.isRequired,
};

const Goal = ({ className, dispatch, board, goal }) => {
  const [isDeleteModalVisible, setDeleteModalVisibility] = React.useState(
    false,
  );
  const completed = goal.tokensAwarded === goal.tokensNeeded;
  const cardClasses = Classnames(
    "goal",
    className,
    completed && "goal--completed",
  );
  const path = `/boards/${board.id}/goals/${goal.id}`;

  const onClickDelete = event => {
    setDeleteModalVisibility(true);
  };

  const onClickHide = () => {
    dispatch(hideGoal(goal.id, board.id));
  };

  const onClickShow = () => {
    dispatch(showGoal(goal.id, board.id));
  };

  const onConfirmDelete = () => {
    dispatch(deleteGoal(goal.id, board.id));
    setDeleteModalVisibility(false);
  };

  const renderControls = isHidden => (
    <div className="goal__controls">
      <Button className="goal__edit-button" theme="link">
        Edit
      </Button>
      <Button
        className="goal__delete-button"
        theme="link"
        onClick={onClickDelete}
      >
        Delete
      </Button>
      <Button
        className="goal__hide-button"
        theme="link"
        onClick={isHidden ? onClickShow : onClickHide}
      >
        {isHidden ? "Show" : "Hide"}
      </Button>
    </div>
  );

  return (
    <React.Fragment>
      {completed && (
        <Card className={cardClasses}>
          <h3 className="goal__reward goal__reward--completed">
            {goal.reward}
          </h3>
          <div className="goal__title">
            <Link
              className="goal__title goal__title--completed goal__link"
              to={path}
            >
              {goal.title}
            </Link>
          </div>
          <div className="goal__tokens goal__tokens--completed">
            {renderTokens(board, goal, dispatch)}
          </div>
          {renderControls(goal.isHidden)}
        </Card>
      )}
      {!completed && (
        <Card className={cardClasses}>
          <h3 className="goal__title">
            <Link className="goal__title goal__link" to={path}>
              {goal.title}
            </Link>
          </h3>
          <div className="goal__reward">Working for: {goal.reward}</div>
          <div className="goal__tokens">
            {renderTokens(board, goal, dispatch)}
          </div>
          {renderControls(goal.isHidden)}
        </Card>
      )}
      <Modal
        className="goal__delete-modal"
        isOpen={isDeleteModalVisible}
        size="small"
        onClose={() => setDeleteModalVisibility(false)}
      >
        <h2 className="goal__delete-modal-header">Delete this goal?</h2>
        <p className="goal__delete-modal-description">
          This will be removed from your list of goals.
        </p>
        <footer className="goal__delete-modal-footer">
          <Button
            className="goal__delete-modal-cancel-button"
            theme="secondary"
            onClick={() => setDeleteModalVisibility(false)}
          >
            Cancel
          </Button>
          <Button theme="delete" onClick={onConfirmDelete}>
            Delete
          </Button>
        </footer>
      </Modal>
    </React.Fragment>
  );
};

Goal.propTypes = propTypes;

export default Goal;

const renderTokens = (board, goal, dispatch) => {
  const { tokensNeeded: total, tokensAwarded: awarded } = goal;

  const onClickToken = event => {
    dispatch(awardToken(goal.id, board.id));
    event.stopPropagation();
  };

  if (total === 0) {
    return <div className="goal__token goal__token--awarded" />;
  }

  let awards = 0;
  let tokens = [];

  for (let i = 0; i < total; i += 1) {
    if (awards < awarded) {
      tokens.push(<div key={i} className="goal__token goal__token--awarded" />);
      awards += 1;
    } else {
      tokens.push(
        <div key={i} className="goal__token" onClick={onClickToken} />,
      );
    }
  }

  return tokens;
};
