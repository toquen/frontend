export const AWARD_TOKEN = "AWARD_TOKEN";
export const BOARD_CREATED = "BOARD_CREATED";
export const CREATE_BOARD = "CREATE_BOARD";
export const GET_BOARDS = "GET_BOARDS";
export const GET_GOALS = "GET_GOALS";
export const GET_TOKENS = "GET_TOKENS";
export const HIDE_GOAL = "HIDE_GOAL";
export const REMOVE_GOAL = "REMOVE_GOAL";
export const REQUEST_HIDE_GOAL = "REQUEST_HIDE_GOAL";
export const SET_ERROR = "SET_ERROR";
export const SET_SELECTED_BOARD = "SET_SELECTED_BOARD";
export const TOGGLE_HIDDEN_GOALS = "TOGGLE_HIDDEN_GOALS";
export const UPDATE_BOARD_TOKENS = "UPDATE_BOARD_TOKENS";
export const UPDATE_BOARDS = "UPDATE_BOARDS";
export const UPDATE_GOALS = "UPDATE_GOALS";
