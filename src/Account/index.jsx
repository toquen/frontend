import React from "react";
import classnames from "classnames";
import { AppState } from "App";
import Button from "components/Button";
import Input from "components/Input";
import "./styles.scss";

const Account = ({ className, match }) => {
  const { api, appState } = React.useContext(AppState);
  const [name, setName] = React.useState(appState.user.name);
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [confirmPassword, setConfirmPassword] = React.useState("");
  const [isPasswordFormVisible, setPasswordFormVisibility] = React.useState(
    false,
  );
  const [saveError, setSaveError] = React.useState(null);

  React.useEffect(() => {
    let abortableRequest;
    let completed;

    const getUser = async () => {
      abortableRequest = api.getMyUser();

      let user;

      try {
        user = await abortableRequest;
      } catch (err) {
        user = { name, email: "" };
      }

      completed = true;

      setName(user.name);
      setEmail(user.email);
    };

    getUser();

    return () => {
      if (!completed) {
        abortableRequest.abort();
      }
    };
  }, [api, name]);

  if (!match) {
    return null;
  }

  const handleSubmit = async e => {
    e.preventDefault();

    setSaveError(null);

    await api
      .updateMyUser({
        name,
        email,
        password,
      })
      .catch(err => {
        setSaveError(err);
      });
  };

  const passwordContainerClassnames = classnames({
    "account__password-container": true,
    "account__password-container--visible": isPasswordFormVisible,
  });
  let arePasswordsMismatched = false;
  let arePasswordsMatched = false;
  let areFieldsPopulated = true;

  if (password !== "" && confirmPassword !== "") {
    if (password !== confirmPassword) {
      arePasswordsMismatched = true;
    } else {
      arePasswordsMatched = true;
    }
  }

  if (
    name === "" ||
    email === "" ||
    (isPasswordFormVisible && (password === "" || confirmPassword === ""))
  ) {
    areFieldsPopulated = false;
  }

  return (
    <div className={`${className} account`}>
      <h2 className="account__header">User Profile</h2>
      <form onSubmit={handleSubmit}>
        <Input
          name="name"
          onChange={({ target }) => setName(target.value)}
          placeholder="Enter your name"
          value={name}
        />
        <Input
          name="email"
          onChange={({ target }) => setEmail(target.value)}
          placeholder="Email address"
          type="email"
          value={email}
        />
        <Button
          className="account__password-toggle"
          onClick={() => setPasswordFormVisibility(!isPasswordFormVisible)}
          label={isPasswordFormVisible ? "Hide" : "Change password"}
          theme="secondary"
        />
        <div className={passwordContainerClassnames}>
          <Input
            className="account__password-field"
            name="password"
            onChange={({ target }) => setPassword(target.value)}
            placeholder="Enter a new password"
            theme={arePasswordsMismatched ? "error" : ""}
            type="password"
            value={password}
          />
          {arePasswordsMismatched && (
            <span
              className="account__password-mismatch-icon"
              role="img"
              aria-label="Passwords mismatched"
            >
              ❌
            </span>
          )}
          {arePasswordsMatched && (
            <span
              className="account__password-match-icon"
              role="img"
              aria-label="Passwords matched"
            >
              ✔️
            </span>
          )}
          <Input
            className="account__password-field"
            name="confirmPassword"
            onChange={({ target }) => setConfirmPassword(target.value)}
            placeholder="Confirm password"
            theme={arePasswordsMismatched ? "error" : ""}
            type="password"
            value={confirmPassword}
          />
          {arePasswordsMismatched && (
            <span
              className="account__password-mismatch-icon"
              role="img"
              aria-label="Passwords mismatched"
            >
              ❌
            </span>
          )}
          {arePasswordsMatched && (
            <span
              className="account__password-match-icon"
              role="img"
              aria-label="Passwords matched"
            >
              ✔️
            </span>
          )}
        </div>
        <div className="account__controls">
          <Button disabled={!areFieldsPopulated} label="Save" type="submit" />
        </div>
      </form>
      {saveError && (
        <div className="account__error">
          Something went wrong and your changes could not be saved
        </div>
      )}
    </div>
  );
};

export default Account;
