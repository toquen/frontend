import React from "react";
import PropTypes from "prop-types";
import Classnames from "classnames";
import "./style.scss";

const COLORS = [
  "cobalt",
  "cream",
  "dust",
  "emerald",
  "graphite",
  "pale",
  "jade",
  "shadow",
  "skyBlue",
  "grape",
  "sunshine",
  "army",
  "danger",
];

const propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  color: PropTypes.oneOf(COLORS),
  onClick: PropTypes.func,
  size: PropTypes.oneOf(["xs", "s", "m", "l", "xl"]),
  tag: PropTypes.string,
};

const Text = props => {
  const { children, className, color, onClick, size = "m", tag = "p" } = props;
  const DynamicTag = tag;
  const classes = Classnames("text", className, {
    "text--xs": size === "xs",
    "text--s": size === "s",
    "text--m": size === "m",
    "text--l": size === "l",
    "text--xl": size === "xl",
    [`text--${color}`]: !!color,
  });

  return (
    <DynamicTag className={classes} onClick={onClick}>
      {children}
    </DynamicTag>
  );
};

Text.propTypes = propTypes;

export default Text;
