import React from "react";
import classnames from "classnames";
import "./styles.scss";

const validThemes = new Set(["error"]);

const Input = ({
  className = "",
  value,
  placeholder,
  name,
  theme,
  type = "text",
  onChange = () => {},
}) => {
  const allClassnames = classnames({
    input: true,
    [`input--${theme}`]: validThemes.has(theme),
  });

  return (
    <input
      className={`${allClassnames} ${className}`}
      type={type}
      name={name}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    />
  );
};

export default Input;
