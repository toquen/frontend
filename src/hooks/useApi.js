import { useCallback, useMemo } from "react";

const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;

const useApi = token => {
  const request = useCallback(
    (method, url, body) => {
      const xhr = new XMLHttpRequest();

      const xhrPromise = new Promise((resolve, reject) => {
        let requestBody;
        try {
          requestBody = JSON.stringify(body);
        } catch (err) {
          return reject(err);
        }

        xhr.onreadystatechange = () => {
          if (xhr.readyState !== 4) {
            return;
          }

          if (xhr.status >= 200 && xhr.status < 300) {
            try {
              resolve(JSON.parse(xhr.responseText));
            } catch (err) {
              reject(new Error(err));
            }
          } else {
            reject(new Error(xhr.status));
          }
        };

        xhr.open(method, url);
        xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
        if (token) {
          xhr.setRequestHeader("Authorization", `Bearer ${token}`);
        }
        xhr.send(requestBody);
      });

      xhrPromise.abort = () => {
        xhr.abort();
      };

      return xhrPromise;
    },
    [token],
  );
  const api = useMemo(
    () => ({
      awardToken: (boardId, goalId) => {
        return request(
          "POST",
          `${API_BASE_URL}/boards/${boardId}/goals/${goalId}/add-token`,
        );
      },

      createBoard: (boardName, learnerName) => {
        return request("POST", `${API_BASE_URL}/boards`, {
          boardName,
          learnerName,
        });
      },

      createGoal: (boardId, goal) => {
        return request("POST", `${API_BASE_URL}/boards/${boardId}/goals`, goal);
      },

      createAccount: user => {
        return request("POST", `${API_BASE_URL}/users/register`, user);
      },

      deleteGoal: async (boardId, goalId) => {
        let success = true;

        await request(
          "DELETE",
          `${API_BASE_URL}/boards/${boardId}/goals/${goalId}`,
        ).catch(() => {
          success = false;
        });

        return success;
      },

      getBoards: () => {
        return request("GET", `${API_BASE_URL}/boards`);
      },

      getTokens: boardId => {
        return request("GET", `${API_BASE_URL}/boards/${boardId}/tokens`);
      },

      getGoals: boardId => {
        return request("GET", `${API_BASE_URL}/boards/${boardId}/goals`);
      },

      getHiddenGoals: boardId => {
        return request("GET", `${API_BASE_URL}/boards/${boardId}/goals/hidden`);
      },

      getMyUser: () => {
        return request("GET", `${API_BASE_URL}/users/me`);
      },

      hideGoal: (boardId, goalId) => {
        return request(
          "POST",
          `${API_BASE_URL}/boards/${boardId}/goals/${goalId}/hide`,
        );
      },

      postAuth: ({ email, password }) => {
        return request("POST", `${API_BASE_URL}/auth`, { email, password });
      },

      refreshAuth: token => {
        return request("POST", `${API_BASE_URL}/auth/refresh`, { token });
      },

      showGoal: (boardId, goalId) => {
        return request(
          "POST",
          `${API_BASE_URL}/boards/${boardId}/goals/${goalId}/show`,
        );
      },

      updateGoal: (boardId, goal) => {
        return request(
          "PUT",
          `${API_BASE_URL}/boards/${boardId}/goals/${goal.id}`,
          goal,
        );
      },

      updateMyUser: user => {
        return request("PUT", `${API_BASE_URL}/users/me`, user);
      },
    }),
    [request],
  );

  return api;
};

export default useApi;
