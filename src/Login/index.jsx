import React, { useContext, useState } from "react";
import { Redirect } from "react-router-dom";
import Input from "components/Input";
import Button from "components/Button";
import { AppState } from "App";
import createDefaultAction from "utils/createDefaultAction";
import { USER_LOGGED_IN } from "appState/actionTypes";
import "./styles.scss";

const Login = ({ className }) => {
  const [error, setError] = useState(null);
  const { api, appDispatch, appState } = useContext(AppState);
  const { postAuth } = api;

  const onSubmit = async event => {
    event.preventDefault();

    const { target } = event;

    const email = target.email.value;
    const password = target.password.value;

    let response;
    try {
      response = await postAuth({ email, password });
    } catch (err) {
      setError(err);
      return;
    }

    const { name, token, userId } = response;
    appDispatch(createDefaultAction(USER_LOGGED_IN, { name, token, userId }));
  };

  return (
    <div className={`login ${className}`}>
      {!appState.user && (
        <form className="login__form" onSubmit={onSubmit}>
          <legend className="login__legend">Log in</legend>
          <Input
            className="login__field"
            placeholder="Email"
            type="email"
            name="email"
          />
          <Input
            className="login__field"
            placeholder="Password"
            type="password"
            name="password"
          />
          {error && <p className="login__error">Invalid email or password</p>}
          <Button className="login__button" type="submit" label="Log in" />
        </form>
      )}
      {appState.user && <Redirect push to="/" />}
    </div>
  );
};

export default Login;
