import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import "./styles.scss";

const validThemes = new Set(["primary", "secondary", "link", "delete"]);

const propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  theme: PropTypes.oneOf(["primary", "secondary", "link", "delete"]),
  type: PropTypes.oneOf(["button", "submit", "reset"]),
};

const Button = ({
  children = null,
  className = "",
  disabled = false,
  label,
  theme = "primary",
  type = "button",
  onClick = () => {},
}) => {
  const allClassnames = classnames({
    button: true,
    [`button--${theme}`]: validThemes.has(theme),
    "button--disabled": disabled,
  });

  return (
    <button
      className={`${allClassnames} ${className}`}
      disabled={disabled}
      onClick={onClick}
      type={type}
    >
      {children || label}
    </button>
  );
};

Button.propTypes = propTypes;

export default Button;
