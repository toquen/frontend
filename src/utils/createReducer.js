const createReducer = handlers => (state, action) => {
  const { type } = action;

  if (!handlers[type]) {
    return state;
  }

  return handlers[type](state, action);
};

export default createReducer;
