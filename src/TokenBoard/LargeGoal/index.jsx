import React from "react";
import "./styles.scss";
import useRequest from "hooks/useRequest";
import useMutation from "hooks/useMutation";
import Text from "components/Text";
import Link from "components/Link";

const LargeGoal = ({ board, goalId }) => {
  const boardId = board && board.id;

  const [goal, setGoal] = React.useState({});
  const { error, loading, data } = useRequest({
    method: "GET",
    path: `/boards/${boardId}/goals/${goalId}`,
    skip: !board || !goalId,
  });
  const awardToken = useMutation({
    method: "POST",
    path: `/boards/${boardId}/goals/${goalId}/add-token`,
  });

  React.useEffect(() => {
    if (!data) {
      return;
    }

    setGoal(awardToken.data || data);
  }, [data, awardToken.data]);

  const completed = goal && goal.tokensAwarded === goal.tokensNeeded;

  const handleClickToken = () => {
    awardToken.request();
  };

  return (
    <div className="large-goal">
      {loading && <Text tag="h1">Loading...</Text>}
      {!loading && !error && data && (
        <React.Fragment>
          <header className="large-goal__header">
            <Text>
              <Link to="/boards">All Token Boards</Link>/
              <Link to={`/boards/${boardId}`}>{board.name}</Link>/
              <Text tag="span">{goal.title}</Text>
            </Text>
          </header>
          <div className="large-goal__body">
            <main className="large-goal__main">
              <Text size="l" tag="h1">
                {goal.title}
              </Text>
              <ul className="large-goal__tokens">
                {getTokens(handleClickToken, goal)}
              </ul>
              {completed && (
                <div className="large-goal__reward large-goal__reward--completed">
                  {goal.reward}
                </div>
              )}
            </main>
            {!completed && (
              <aside className="large-goal__reward">
                Working for:&nbsp;
                <Text color="grape" size="s">
                  {goal.reward}
                </Text>
              </aside>
            )}
          </div>
        </React.Fragment>
      )}
    </div>
  );
};

export default LargeGoal;

const getTokens = (handleClickToken, goalData) => {
  if (!goalData) {
    return null;
  }

  const { tokensAwarded, tokensNeeded } = goalData;
  const tokens = [];

  for (let i = 0; i < tokensNeeded; i += 1) {
    if (i < tokensAwarded) {
      tokens.push(
        <div
          className="large-goal__token large-goal__token--awarded"
          role="button"
          key={i}
        >
          {i + 1}
        </div>,
      );
    } else {
      tokens.push(
        <div
          className="large-goal__token"
          role="button"
          key={i}
          onClick={handleClickToken}
        >
          {i + 1}
        </div>,
      );
    }
  }

  return tokens;
};
