export const INIT = "INIT";
export const INITIALIZED = "INITIALIZED";
export const SET_AUTH_TOKEN = "SET_AUTH_TOKEN";
export const USER_LOGGED_IN = "USER_LOGGED_IN";
export const USER_LOGGED_OUT = "USER_LOGGED_OUT";
