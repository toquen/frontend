import createMiddleware from "utils/createMiddleware";
import {
  DELETE_GOAL,
  GOAL_DELETED,
  CREATE_GOAL,
  GOAL_CREATED,
  TOGGLE_NEW_GOAL_MODAL,
  AWARD_TOKEN,
  GOAL_UPDATED,
  HIDE_GOAL,
  SHOW_GOAL,
} from "./actionTypes";
import createDefaultAction from "utils/createDefaultAction";

export default (api, refetchGoals, refetchHiddenGoals) => {
  const handlers = {
    [AWARD_TOKEN]: async (dispatch, state, { payload }) => {
      const { boardId, goalId } = payload;

      const response = await api.awardToken(boardId, goalId).catch(err => {
        return null;
      });

      if (response) {
        dispatch(createDefaultAction(GOAL_UPDATED, response));
      }
    },

    [CREATE_GOAL]: async (dispatch, state, { payload }) => {
      const { boardId, ...rest } = payload;
      await api.createGoal(boardId, rest);

      dispatch(createDefaultAction(GOAL_CREATED));
    },

    [DELETE_GOAL]: async (dispatch, state, { payload }) => {
      await api.deleteGoal(payload.boardId, payload.goalId);

      dispatch(createDefaultAction(GOAL_DELETED, payload.goalId));
    },

    [GOAL_CREATED]: async dispatch => {
      await refetchGoals();

      dispatch(createDefaultAction(TOGGLE_NEW_GOAL_MODAL));
    },

    [GOAL_DELETED]: () => {
      return refetchGoals();
    },

    [HIDE_GOAL]: async (dispatch, state, { payload }) => {
      await api.hideGoal(payload.boardId, payload.goalId);
      return Promise.all([refetchGoals(), refetchHiddenGoals()]);
    },

    [SHOW_GOAL]: async (dispatch, state, { payload }) => {
      await api.showGoal(payload.boardId, payload.goalId);
      return Promise.all([refetchGoals(), refetchHiddenGoals()]);
    },
  };

  return createMiddleware(handlers);
};
