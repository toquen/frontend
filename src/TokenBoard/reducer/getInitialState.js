const getInitialState = () => {
  return {
    boards: null,
    error: null,
    selectedBoard: null,
  };
};

export default getInitialState;

export const getInitialGoalState = goal => {
  const { isHidden, id, title, tokensAwarded, tokensNeeded, reward } = goal;

  return {
    isHidden,
    id,
    reward,
    title,
    tokensAwarded,
    tokensNeeded,
  };
};

export const getInitialTokenState = token => {
  if (token.id === "DEFAULT") {
    return {
      ...token,
      icon: "smiley",
      name: "Smiley",
    };
  } else {
    return token;
  }
};

export const getInitialBoardState = board => {
  const { id, name, goals = [], tokens = [] } = board;

  return {
    id,
    goals: goals.reduce((acc, goal) => {
      const goalState = getInitialGoalState(goal);
      acc[goalState.id] = goalState;
      return acc;
    }, {}),
    name,
    showHiddenGoals: false,
    tokens: tokens.reduce((acc, token) => {
      const tokenState = getInitialTokenState(token);
      acc[tokenState.id] = tokenState;
      return acc;
    }, {}),
  };
};
