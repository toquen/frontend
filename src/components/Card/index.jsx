import React from "react";
import PropTypes from "prop-types";
import Classnames from "classnames";
import "./style.scss";

const propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
};

const Card = props => {
  const { children, className, onClick } = props;
  const classes = Classnames("card", className);

  return (
    <div className={classes} onClick={onClick}>
      {children}
    </div>
  );
};

Card.propTypes = propTypes;

export default Card;
