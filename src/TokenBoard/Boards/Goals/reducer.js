import createReducer from "utils/createReducer";
import {
  TOGGLE_HIDDEN_GOALS,
  TOGGLE_NEW_GOAL_MODAL,
  GOALS_RESPONSE_RECEIVED,
  HIDDEN_GOALS_RESPONSE_RECEIVED,
  GOAL_UPDATED,
  GOAL_DELETED,
} from "./actionTypes";

const handlers = {
  [GOAL_DELETED]: (state, { payload: goalId }) => {
    const { goals } = state;
    const { [goalId]: goalToRemove, ...restOfGoals } = goals;

    return {
      ...state,
      goals: restOfGoals,
    };
  },

  [GOAL_UPDATED]: (state, { payload: goal }) => ({
    ...state,
    goals: {
      ...state.goals,
      [goal.id]: goal,
    },
  }),

  [GOALS_RESPONSE_RECEIVED]: (state, { payload }) => ({
    ...state,
    goals: (payload || []).reduce((acc, goal) => {
      acc[goal.id] = goal;
      return acc;
    }, {}),
  }),

  [HIDDEN_GOALS_RESPONSE_RECEIVED]: (state, { payload }) => ({
    ...state,
    hiddenGoals: (payload || []).reduce((acc, goal) => {
      acc[goal.id] = goal;
      return acc;
    }, {}),
  }),

  [TOGGLE_HIDDEN_GOALS]: state => ({
    ...state,
    showHiddenGoals: !state.showHiddenGoals,
  }),

  [TOGGLE_NEW_GOAL_MODAL]: state => ({
    ...state,
    showNewGoalModal: !state.showNewGoalModal,
  }),
};

export default createReducer(handlers);

export const getInitialState = () => {
  return {
    goals: null,
    hiddenGoals: null,
    showHiddenGoals: false,
    showNewGoalModal: false,
  };
};
