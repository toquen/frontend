import {
  USER_LOGGED_IN,
  INIT,
  USER_LOGGED_OUT,
  INITIALIZED,
} from "./actionTypes";
import createDefaultAction from "utils/createDefaultAction";

const getMiddleware = apiRef => {
  const handlers = {
    [INIT]: async (dispatch, state) => {
      const authToken = localStorage.getItem("authToken");

      if (authToken) {
        let result;

        try {
          result = await apiRef.current.refreshAuth(authToken);
        } catch (e) {
          localStorage.removeItem("authToken");
          dispatch(createDefaultAction(INITIALIZED));
          return;
        }

        const { name, token, userId } = result;

        // Update localStorage with refreshed token
        localStorage.setItem("authToken", token);

        const userLoggedInAction = createDefaultAction(USER_LOGGED_IN, {
          name,
          token,
          userId,
        });

        dispatch(userLoggedInAction);
      } else {
        dispatch(createDefaultAction(INITIALIZED));
      }
    },

    [USER_LOGGED_OUT]: (dispatch, state) => {
      localStorage.removeItem("authToken");
    },

    [USER_LOGGED_IN]: (dispatch, state, { payload }) => {
      localStorage.setItem("authToken", payload.token);
      dispatch(createDefaultAction(INITIALIZED));
    },
  };

  return (dispatch, state, action) => {
    const { type } = action;

    if (type in handlers) {
      handlers[type](dispatch, state, action);
    }
  };
};

export default getMiddleware;
