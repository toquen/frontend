export default () => ({
  authToken: null,
  initialized: false,
  user: null,
});
