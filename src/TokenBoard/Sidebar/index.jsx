import React from "react";
import Link from "components/Link";
import "./styles.scss";

const Sidebar = ({ className, boards }) => {
  return (
    <div className={`sidebar ${className}`}>
      <h2 className="sidebar__header">Latest Boards</h2>
      {!boards && <h2 className="sidebar__header">Loading boards...</h2>}
      {boards && (
        <ul>
          {Object.values(boards).map(board => (
            <li className="sidebar__link" key={board.id}>
              <Link to={`/boards/${board.id}`}>{board.name}</Link>
            </li>
          ))}
        </ul>
      )}
      <hr className="sidebar__divider" />
      <Link className="sidebar__link" to="/goals/tokens">
        Manage tokens
      </Link>
    </div>
  );
};

export default Sidebar;
