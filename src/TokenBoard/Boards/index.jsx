import React from "react";
import PropTypes from "prop-types";
import { Switch, Route } from "react-router-dom";
import Text from "components/Text";
import Card from "components/Card";
import Link from "components/Link";
import Button from "components/Button";
import LargeGoal from "TokenBoard/LargeGoal";
import Goals from "./Goals";
import "./styles.scss";
import NewBoardModal from "./NewBoardModal";

const propTypes = {
  boards: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
};

const Boards = ({ boards, dispatch }) => {
  const [showNewBoardModal, setNewBoardModalVisibility] = React.useState(false);

  const toggleModalVisibility = () => {
    setNewBoardModalVisibility(visibility => !visibility);
  };

  return (
    <div className="boards flex-1 overflow-y-scroll">
      <Switch>
        <Route
          path="/boards/:boardId/goals/:goalId"
          render={({ match }) => {
            const { boardId, goalId } = match.params;

            return (
              <LargeGoal board={boards && boards[boardId]} goalId={goalId} />
            );
          }}
        />
        <Route
          path="/boards/:boardId"
          render={({ match }) => {
            const board = boards && boards[match.params.boardId];

            if (!board) {
              return (
                <Text size="l" tag="h1">
                  Board not found
                </Text>
              );
            }

            return <Goals board={board} />;
          }}
        />
        <Route
          path="/boards"
          render={() => (
            <React.Fragment>
              <Text size="l" tag="h1">
                All Token Boards
              </Text>
              <Button onClick={toggleModalVisibility} size="m">
                Add New Board
              </Button>
              <ul className="boards__list">
                {Object.values(boards || {}).map(board => (
                  <li key={board.id}>
                    <Link className="boards__link" to={`/boards/${board.id}`}>
                      <Card className="boards__card">
                        <Text size="m" tag="h2">
                          {board.name}
                        </Text>
                      </Card>
                    </Link>
                  </li>
                ))}
              </ul>
              <NewBoardModal
                dispatch={dispatch}
                isOpen={showNewBoardModal}
                onClose={toggleModalVisibility}
              />
            </React.Fragment>
          )}
        />
      </Switch>
    </div>
  );
};

Boards.propTypes = propTypes;

export default Boards;
