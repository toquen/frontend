import React from "react";

const useReducerWithMiddleware = (reducer, middlewares, initialState, init) => {
  const [actionsToDispatch, setActionsToDispatch] = React.useState([]);
  const memoizedReducer = React.useCallback(
    (middleState, action) => {
      middlewares.forEach(fn => {
        fn(
          // middleware functions are passed a "dispatch" function that pushes
          // actions to `actionsToDispatch`. Otherwise, calling `dispatch`
          // directly in a middleware function will lead to stale state for the
          // reducer.
          middlewareAction => {
            setActionsToDispatch(actions => actions.concat(middlewareAction));
          },
          middleState,
          action,
        );
      });

      return reducer(middleState, action);
    },
    [middlewares, reducer],
  );
  const [state, dispatch] = React.useReducer(
    memoizedReducer,
    initialState,
    init,
  );

  React.useEffect(() => {
    if (actionsToDispatch.length) {
      // Clear queued actions
      setActionsToDispatch([]);

      actionsToDispatch.forEach(action => dispatch(action));
    }
  }, [state, actionsToDispatch]);

  return [state, dispatch];
};

export default useReducerWithMiddleware;
