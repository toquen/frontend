import React from "react";
import { AppState } from "App";

const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;

const useMutation = ({ method, path }) => {
  const ref = React.useRef();
  const {
    appState: { authToken },
  } = React.useContext(AppState);
  const [status, setStatus] = React.useState({
    error: null,
    loading: false,
    data: null,
  });

  if (!ref.current) {
    ref.current = new Requester(setStatus);
  }

  React.useEffect(() => ref.current.getAbortCallback(), []);

  return ref.current.startExecution(method, path, status, authToken);
};

export default useMutation;

class Requester {
  constructor(setStatus) {
    this._setStatus = setStatus;
    this._request = this._request.bind(this);
  }

  startExecution(method, path, status, authToken) {
    this._requestParams = { method, path, authToken };

    return {
      request: this._request,
      ...status,
    };
  }

  getAbortCallback() {
    return () => {
      if (this._requestInProgress && this._requestInProgress.abort) {
        this._requestInProgress.abort();
      }
    };
  }

  _request(body) {
    const { method, path, authToken } = this._requestParams;
    const xhr = new XMLHttpRequest();

    this._requestInProgress = xhr;
    this._setStatus({ error: null, loading: true });

    xhr.onreadystatechange = () => {
      if (xhr.readyState !== 4) {
        return;
      }

      if (xhr.status < 200 && xhr.status >= 400) {
        this._setStatus(status => ({
          ...status,
          error: new Error(xhr.status),
        }));
      }

      this._setStatus(status => ({
        ...status,
        loading: false,
        data: xhr.response,
      }));
    };

    xhr.open(method, `${API_BASE_URL}${path}`);
    xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
    if (authToken) {
      xhr.setRequestHeader("Authorization", `Bearer ${authToken}`);
    }
    xhr.responseType = "json";
    xhr.send(body);
  }
}
